package minpro289.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="t_appointment")

public class T_appointment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long Id;
	
	@ManyToOne
	@JoinColumn(name="costumer_id" ,insertable=false, updatable=false)
	public M_customer m_customer;
	
	@ManyToOne
	@JoinColumn(name="doctor_office_id" ,insertable=false, updatable=false)
	public T_doctor_office t_doctor_office;
	
	@ManyToOne
	@JoinColumn(name="doctor_office_schedule_id" ,insertable=false, updatable=false)
	public T_doctor_office_schedule t_doctor_office_schedule;
	
	@ManyToOne
	@JoinColumn(name="doctor_office_treatment_id" ,insertable=false, updatable=false)
	public T_doctor_office_treatment t_doctor_office_treatment;

	@NotNull
	@Column(name="appointment_date")
	private LocalDateTime Appointment_date;
	

	@ManyToOne
	@JoinColumn(name="created_by",insertable=false, updatable=false)
	public M_user User_create;
	
	@NotNull
	@Column(name="created_by")
	private long Created_by;

	@ManyToOne
	@JoinColumn(name="modified_by",insertable=false, updatable=false)
	public M_user User_modified;
	
	@Nullable
	@Column(name="modified_by")
	private long Modified_by;

	@ManyToOne
	@JoinColumn(name="deleted_by",insertable=false, updatable=false)
	public M_user User_deleted;
	
	@Nullable
	@Column(name="deleted_by")
	private long Deleted_by;
	
	@NotNull
	@Column(name="is_delete",columnDefinition = "boolean default false")
	private boolean Is_delete;
	
	
}
