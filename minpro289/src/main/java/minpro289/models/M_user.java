package minpro289.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="m_user")

public class M_user {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long Id;
	
	@ManyToOne
	@JoinColumn(name="biodata_id" ,insertable=false, updatable=false)
	public M_biodata m_biodata;
	
	@ManyToOne
	@JoinColumn(name="role_id", insertable=false, updatable=false)
	public M_role m_role;
	
	@NotNull
	@Column(name="email", length = 100)
	private String Email;
	
	@NotNull
	@Column(name="password", length = 100)
	private String Password;
	
	@NotNull
	@Column(name="login_attemp")
	private long Login_attemp;
	
	@NotNull
	@Column(name="is_locked")
	private Boolean Is_locked;

	@NotNull
	@Column(name="last_login")
	private LocalDateTime Last_login;
	
	@ManyToOne
	@JoinColumn(name="created_by",insertable=false, updatable=false)
	public M_user User_create;
	
	@NotNull
	@Column(name="created_by")
	private long Created_by;

	@ManyToOne
	@JoinColumn(name="modified_by",insertable=false, updatable=false)
	public M_user User_modified;
	
	@Nullable
	@Column(name="modified_by")
	private long Modified_by;

	@ManyToOne
	@JoinColumn(name="deleted_by",insertable=false, updatable=false)
	public M_user User_deleted;
	
	@Nullable
	@Column(name="deleted_by")
	private long Deleted_by;
	
	@NotNull
	@Column(name="is_delete",columnDefinition = "boolean default false")
	private boolean Is_delete;
	

}
